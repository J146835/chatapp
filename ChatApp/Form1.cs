﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using System.Net.Sockets;
using System.IO;

namespace ChatApp
{
    public partial class Form1 : Form
    {

        private TcpClient client;
        public StreamReader sr;
        public StreamWriter sw;
        public string receive;
        public string message;

        // Default port for quicker config
        public int defaultPort = 1337;

        KeyCounter keyCounter = new KeyCounter();

        public Form1()
        {
            InitializeComponent();

            // adds the keypress event handler from the KeyCounter class to txtKeyCount
            txtKeyCount.KeyPress += new KeyPressEventHandler(keyCounter.myKeyCounter);
            txtKeyCount.Text = "Click here, type then press enter to count key presses...\n";

            // Gets local IP address(es)
            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());

            // adds found IPs to the appropriate textboxes
            foreach (IPAddress address in localIP)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    txtServerIp.Text = address.ToString();
                    txtClientIp.Text = address.ToString();
                }
            }
            // Sets the defaultPort to the port textboxes
            txtClientPort.Text = defaultPort.ToString();
            txtServerPort.Text = defaultPort.ToString();
        }

        // Starts Server - seems to freeze the application until the client connects
        private void btnServerStart_Click(object sender, EventArgs e)
        {
            modeToolStripMenuItem.Enabled = false;
            btnServerStart.Enabled = false;
            TcpListener listener = new TcpListener(IPAddress.Any, int.Parse(txtServerPort.Text));
            listener.Start();
            client = listener.AcceptTcpClient();
            sr = new StreamReader(client.GetStream());
            sw = new StreamWriter(client.GetStream());
            sw.AutoFlush = true;

            backgroundWorker1.RunWorkerAsync();
            backgroundWorker2.WorkerSupportsCancellation = true;
            txtChatLog.AppendText("Client has connected" + "\n");
            // Enables the send button which would otherwise cause an error if clicked when not connected
            btnSend.Enabled = true;
        }

        // Attempts to connect to a running server
        private void btnClientConnect_Click(object sender, EventArgs e)
        {
            modeToolStripMenuItem.Enabled = false;
            btnClientConnect.Enabled = false;
            client = new TcpClient();
            IPEndPoint IpEnd = new IPEndPoint(IPAddress.Parse(txtClientIp.Text), int.Parse(txtClientPort.Text));

            try
            {
                client.Connect(IpEnd);

                if (client.Connected)
                {
                    txtChatLog.AppendText("Connected to server" + "\n");
                    sw = new StreamWriter(client.GetStream());
                    sr = new StreamReader(client.GetStream());
                    sw.AutoFlush = true;
                    backgroundWorker1.RunWorkerAsync();
                    backgroundWorker2.WorkerSupportsCancellation = true;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                btnClientConnect.Enabled = true;
                modeToolStripMenuItem.Enabled = true;
            }
            // Enables the send button which would otherwise cause an error if clicked when not connected
            btnSend.Enabled = true;
        }

        // Listens and receives clients messages
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            while (client.Connected)
            {
                try
                {
                    receive = sr.ReadLine();
                    this.txtChatLog.Invoke(new MethodInvoker(delegate ()
                    {
                        txtChatLog.AppendText("Not Me> " + receive + "\n");
                    }));
                    receive = "";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        // sends message if a client is connected and the message textbox is not empty
        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            if (client.Connected && message != "")
            {
                sw.WriteLine(message);
                this.txtChatLog.Invoke(new MethodInvoker(delegate ()
                {
                    txtChatLog.AppendText("Me> " + message + "\n");
                }));
            }
            else
            {
                MessageBox.Show("Sending failure");
            }
            backgroundWorker2.CancelAsync();
        }

        // Sends the text in the message textbox
        private void btnSend_Click(object sender, EventArgs e)
        {
            if (txtMessage.Text != "")
            {
                message = txtMessage.Text;
                backgroundWorker2.RunWorkerAsync();
            }
            txtMessage.Text = "";
        }

        // Changes controls to Server mode
        private void serverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Chat Application - Server";
            grpClient.Enabled = false;
            grpClient.Visible = false;
            grpServer.Enabled = true;
            grpServer.Visible = true;
        }

        // Changes controls to Client mode
        private void clientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = "Chat Application - Client";
            grpClient.Enabled = true;
            grpClient.Visible = true;
            grpServer.Enabled = false;
            grpServer.Visible = false;
        }

        // Allows the user to send message by pressing enter
        private void txtMessage_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)

            {
                btnSend.PerformClick();
            }
        }

        // Activates the output of the KeyCount class
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)

            {
                txtKeyCount.Text = "Click here, type then press enter to count key presses...\n" 
                    + keyCounter.output + "\n";
            }
        }
    }
}
